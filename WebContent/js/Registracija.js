$(document).ready(function() { 
	var ime = $('#imeInput');
	var prezime = $('#prezimeInput');
	var pol = $('#polInput');
	var korisnicko = $('#korisnickoInput');
	var lozinka = $('#lozinkaInput');
	var adresa = $('#adresaInput');
	var brTelefona = $('#brTelefonaInput');
	
	$('#registracijaSubmit').on('click', function(event) { // izvršava se na klik na dugme

		var ime = imeInput.val();
		var prezime = prezimeInput.val();
		var pol = polInput.val();
		var korisnicko = korisnickoInput.val();
		var lozinka = lozinkaInput.val();
		var adresa = adresaInput.val();
		var brTelefona = brTelefonaInput.val();
		
	/*	
		console.log('userName: ' + userName);
		console.log('password: ' + password);
		console.log('type: ' + type);
	 */
		var params = {

			'ime':  ime,
			'prezime' : prezime,
			'pol' : pol,
			'korisnicko' : korisnicko,
			'lozinka' : lozinka,
			'adresa' : adresa,
			'brTelefona' : brTelefona
				
		}
		// kontrola toka se račva na 2 grane
		$.post('RegistracijaServlet', params, function(data) { // u posebnoj programskoj niti se šalje (asinhroni) zahtev
			// tek kada stigne odgovor izvršiće se ova anonimna funkcija
			console.log('stigao odgovor!')
			console.log(data);

			if (data.status == 'failure') {
				imeInput.val('');
				prezimeInput.val('');
				polInput.val('');
				korisnickoInput.val('');
				lozinkaInput.val('');
				adresaInput.val('');
				brTelefonaInput.val('');
				type.val('');

				return;
			}
			if (data.status == 'success') {
				//window.location.replace('WebShop.html');
			}
		});
		// program se odmah nastavlja dalje, pre nego što stigne odgovor
		console.log('poslat zahtev!')

		// zabraniti da browser obavi podrazumevanu akciju pri događaju
		event.preventDefault();
		return false;
	});
});