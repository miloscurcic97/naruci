package DAO;
import DAO.Connect;
import modeli.Artikal;
import modeli.Dostavljac;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class JeloDAO  {
	static Connection con = null;
	
	//private ArrayList<Artikal> artikli;
	
	public static ArrayList<Artikal> getAllArtikal() {
		ArrayList<Artikal> list = new ArrayList<Artikal>();
		con = Connect.getConnection();
		
		try {
			
			PreparedStatement ps = con.prepareStatement("select * from artikli");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Artikal r = new Artikal();
				r.setRestoran(rs.getString(1));
				r.setNaziv(rs.getString(2));
				r.setCena(rs.getDouble(3));
				r.setOpis(rs.getString(4));
				r.setKolicina(rs.getDouble(5));
				r.setId(rs.getString(6));
				list.add(r);
			}
			//con.close();
			
			
		}catch(Exception e) {e.printStackTrace();}
		
		return list;
	}
	/*
	public static Artikal getArtikalByID(String id ) {
		Artikal r = new Artikal(null, null, 0, null, 0,null);
		con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from jela where naziv=?");
			ResultSet rs = ps.executeQuery();
			r.setRestoran(rs.getString(1));
			r.setNaziv(rs.getString(2));
			r.setCena(rs.getDouble(3));
			r.setOpis(rs.getString(2));
			r.setKolicina(rs.getDouble(3));
			
		}catch(Exception e) {e.printStackTrace();;}
		
		return r;
	}
	*/
	
	public static  boolean createArtikal(Artikal r) {
		con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("insert into artikli values(?,?,?,?,?,?)");
			ps.setString(1, r.getRestoran());
			ps.setString(2, r.getNaziv());
			ps.setDouble(3, r.getCena());
			ps.setString(4, r.getOpis());
			ps.setDouble(5, r.getKolicina());
			ps.setString(6, r.getId());
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}

	public static boolean updateArtikal(Artikal r) {
		int status = 0;
		con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("update artikli set restoran=?, naziv=?, cena=?, opis=?, kolicina=? where id =?");
			ps.setString(1, r.getRestoran());
			ps.setString(2, r.getNaziv());
			ps.setDouble(3, r.getCena());
			ps.setString(4, r.getOpis());
			ps.setDouble(5, r.getKolicina());
			ps.setString(6, r.getId());
			return ps.executeUpdate() == 1;
		}catch(Exception e) {e.printStackTrace();}
		
		return false;
		
	}
	
	public static boolean deleteArtikal(String id) {
		
		con = Connect.getConnection();
		
		try {
			PreparedStatement ps = con.prepareStatement("delete artikli where id =?");
			ps.setString(1, id);
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		
		return false;
	}

	public static Artikal getArtikal(String id) {
		
		Connection con = Connect.getConnection();
	//	PreparedStatement pstmt = null;
	//	ResultSet rset = null;
		try {
			PreparedStatement ps = con.prepareStatement("select * from artikli where id =?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				//Role role = Role.valueOf(rset.getString(1));
				String restoran = String.valueOf(rs.getString(1));
				String naziv = String.valueOf(rs.getString(2));
				Double cena = Double.parseDouble(String.valueOf(rs.getString(3)));
				String opis = String.valueOf(rs.getString(4));
				Double kolicina = Double.parseDouble(String.valueOf(rs.getString(5)));
				String idArtikla = String.valueOf(rs.getString(6));
				
				return new Artikal(restoran, naziv, cena, opis, kolicina, idArtikla);

			//	String id = String.valueOf(rs.getString(8));
			//	return new Dostavljac(ime, prezime, pol, korisnickoIme, lozinka, registracija, tipVozila,id);
			}
		//	con.close();
		}catch(Exception e) {e.printStackTrace();}
		return null;
	}
	
	public static ArrayList<Artikal> getRestoranMenu(String id) {
		ArrayList<Artikal> list = new ArrayList<Artikal>();
		con = Connect.getConnection();
		
		try {
			
			PreparedStatement ps = con.prepareStatement("select * from artikli where restoran=?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Artikal r = new Artikal();
				r.setRestoran(rs.getString(1));
				r.setNaziv(rs.getString(2));
				r.setCena(rs.getDouble(3));
				r.setOpis(rs.getString(4));
				r.setKolicina(rs.getDouble(5));
				r.setId(rs.getString(6));
				list.add(r);
			}
			//con.close();
			
			
		}catch(Exception e) {e.printStackTrace();}
		
		return list;
	}
	
	
}