package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import DAO.Connect;
import modeli.Administrator;
import modeli.Porudzbina;
import modeli.Restoran;

public class PorudzbinaDAO {
	
	private ArrayList<Porudzbina> porudzbine;
	
	public PorudzbinaDAO () {
		this.porudzbine = new ArrayList<Porudzbina>();
		
	}
	
	public static ArrayList<Porudzbina> getAllPorudzbina() {
		ArrayList<Porudzbina> porudzbine = new ArrayList<Porudzbina>();
		Connection con = Connect.getConnection();	
		try {
			
			PreparedStatement ps = con.prepareStatement("select * from porudzbine");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Porudzbina r = new Porudzbina("","","","","",0,"",0, "");
				r.setJelo(rs.getString(1));
				r.setDatum(rs.getString(2));
				r.setDostavljac(rs.getString(3));
				r.setKupac(rs.getString(4));
				r.setRestoran(rs.getString(5));
				r.setBroj(rs.getInt(6));
				r.setStanje(rs.getString(7));
				r.setCena(rs.getDouble(8));
				r.setId(rs.getString(9));
				porudzbine.add(r);
			}
			//con.close();		
		}catch(Exception e) {e.printStackTrace();}
		
		return porudzbine;
	}

	public static ArrayList<Porudzbina> getPorudzbinaKupacID(String id) {
		
		ArrayList <Porudzbina> porudzbine = new ArrayList<>();
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from porudzbine where kupac=?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Porudzbina r = new Porudzbina("","","","","",0,"",0,"");
				r.setJelo(rs.getString(1));
				r.setDatum(rs.getString(2));
				r.setDostavljac(rs.getString(3));
				r.setKupac(rs.getString(4));
				r.setRestoran(rs.getString(5));
				r.setBroj(rs.getInt(6));
				r.setStanje(rs.getString(7));
				r.setCena(rs.getDouble(8));
				r.setId(rs.getString(9));
				porudzbine.add(r);
			}
			
		}catch(Exception e) {e.printStackTrace();;}
		return porudzbine;

	}
	

	
	
	public static boolean createPorudzbina(Porudzbina r) {
	
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("insert into porudzbine values(?,?,?,?,?,?,?,?,?)");
			ps.setString(1, r.getJelo());
			ps.setString(2, r.getDatum());
			ps.setString(3, r.getDostavljac());
			ps.setString(4, r.getKupac());
			ps.setString(5, r.getRestoran());
			ps.setInt(6, r.getBroj());
			ps.setString(7, r.getStanje());
			ps.setDouble(8, r.getCena());
			ps.setString(9, r.getId());
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}

	public static boolean updatePorudzbina(String stanje, String id) {
		
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("update porudzbine set stanje=? where id=?");
			ps.setString(1, stanje);
			ps.setString(2, id);
			System.out.println("\n");
			System.out.println("xxxxxxxxxxxxxx");
			System.out.println("\n");
			System.out.println(stanje);
			System.out.println("\n");
			System.out.println(ps);
			System.out.println("\n");
			System.out.println(id);
			System.out.println("\n");

			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		
		return false;
		
	}
	
	
	public static boolean deletePorudzbine(String id) {
		
		Connection con = Connect.getConnection();	
		try {
			
			PreparedStatement ps = con.prepareStatement("delete from porudzbine where id =?");
			ps.setString(1, id);
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		return false;
		
		//return status;
	}	
	
	
	public void removePorudzbina(Porudzbina porudzbina) {
		
		porudzbine.remove(porudzbina);
	}

	
	public static ArrayList<Porudzbina> getPorudzbinaCekanje() {
		ArrayList <Porudzbina> porudzbine = new ArrayList<>();
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from porudzbine where (stanje is null or stanje = 'ceka') ");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Porudzbina r = new Porudzbina("","","","","",0,"",0,"");
				r.setJelo(rs.getString(1));
				r.setDatum(rs.getString(2));
				r.setDostavljac(rs.getString(3));
				r.setKupac(rs.getString(4));
				r.setRestoran(rs.getString(5));
				r.setBroj(rs.getInt(6));
				r.setStanje(rs.getString(7));
				r.setCena(rs.getDouble(8));
				r.setId(rs.getString(9));
				porudzbine.add(r);
			}
			
		}catch(Exception e) {e.printStackTrace();;}
		return porudzbine;
	}

	public static ArrayList<Porudzbina> getPorudzbinaStare(String id) {
		ArrayList <Porudzbina> porudzbine = new ArrayList<>();
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from porudzbine where stanje like 'dostavljeno' and id=? ");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Porudzbina r = new Porudzbina("","","","","",0,"",0,"");
				r.setJelo(rs.getString(1));
				r.setDatum(rs.getString(2));
				r.setDostavljac(rs.getString(3));
				r.setKupac(rs.getString(4));
				r.setRestoran(rs.getString(5));
				r.setBroj(rs.getInt(6));
				r.setStanje(rs.getString(7));
				r.setCena(rs.getDouble(8));
				r.setId(rs.getString(9));
				System.out.print("\n");
				System.out.print(r);
				System.out.print("\n");
				porudzbine.add(r);
			}
			
		}catch(Exception e) {e.printStackTrace();;}
		return porudzbine;
	} 
	
	
	public static Porudzbina getPorudzbina(String id) {
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from porudzbine where id=? ");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				String naziv = String.valueOf(rs.getString(1));
				String datum = String.valueOf(rs.getString(2));
				String dostavljac = String.valueOf(rs.getString(3));
				String kupac = String.valueOf(rs.getString(4));
				String restoran = String.valueOf(rs.getString(5));
				Integer broj = Integer.parseInt(String.valueOf(rs.getInt(6))) ;
				String stanje = String.valueOf(rs.getString(7));
				Double cena = Double.parseDouble(String.valueOf(rs.getDouble(8))) ;
				String idP = String.valueOf(rs.getString(9));
				System.out.print("~~~~~~~~~~");
				return new Porudzbina(naziv,datum,dostavljac,kupac,restoran,broj,stanje,cena,idP);
				
			}
			
		}catch(Exception e) {e.printStackTrace();;}
		return null;

	} 

}
