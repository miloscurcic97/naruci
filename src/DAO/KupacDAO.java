package DAO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import DAO.Connect;
import modeli.Dostavljac;
import modeli.Kupac;
import modeli.Restoran;
import modeli.Kupac;

public class KupacDAO {
	
	private static ArrayList<Kupac> kupci;
	
	public KupacDAO() {
		this.kupci = new ArrayList<Kupac>();
	}
	
	public ArrayList<Kupac> getKupac() {
		return kupci;
		
	}

	public static ArrayList<Kupac> getAllKupci() {
		ArrayList<Kupac> kupci = new ArrayList<Kupac>();
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from kupci");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				Kupac d = new Kupac(null, null, null, null, null, null, null,null);
				d.setIme(rs.getString(1));
				d.setPrezime(rs.getString(2));
				d.setPol(rs.getString(3));
				d.setKorisnicko(rs.getString(4));
				d.setLozinka(rs.getString(5));
				d.setAdresa(rs.getString(6));
				d.setBrTelefona(rs.getString(7));
				d.setId(rs.getString(8));
				kupci.add(d);
				System.out.print(d);
			}
			//con.close();
		}catch(Exception e) {e.printStackTrace();}
		
		return kupci;
		
	}
	
	public static Kupac getKupac(String userName, String password) {
		Connection conn = Connect.getConnection();
		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT * FROM kupci WHERE korisnicko = ? AND lozinka = ?";
			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, userName);
			pstmt.setString(index++, password);
			System.out.println(pstmt);
			rset = pstmt.executeQuery();

			if (rset.next()) {
				/*
				Kupac d = new Kupac(null, null, null, null, null, null, null,null);
				d.setIme(rset.getString(1));
				d.setPrezime(rset.getString(2));
				d.setPol(rset.getString(3));
				d.setKorisnicko(rset.getString(4));
				d.setLozinka(rset.getString(5));
				d.setAdresa(rset.getString(6));
				d.setBrTelefona(rset.getString(7));
				d.setId(rset.getString(8));
				return d;
	*/			
				//Role role = Role.valueOf(rset.getString(1));
				String ime = String.valueOf(rset.getString(1));
				String prezime = String.valueOf(rset.getString(2));
				String pol = String.valueOf(rset.getString(3));
				String korisnicko = String.valueOf(rset.getString(4));
				String lozinka = String.valueOf(rset.getString(5));
				String adresa = String.valueOf(rset.getString(6));
				String brTelefona = String.valueOf(rset.getString(7));
				String id = String.valueOf(rset.getString(8));
				System.out.println("----------");
				System.out.println(id);
				System.out.println("----------");
				return new Kupac(ime, prezime,pol ,korisnicko,lozinka,adresa ,brTelefona, id);
				
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
		//	try {pstmt.close();} catch (Exception ex1) {ex1.printStackTrace();}
		//	try {rset.close();} catch (Exception ex1) {ex1.printStackTrace();}
		//	try {conn.close();} catch (Exception ex1) {ex1.printStackTrace();}
		}

		return null;
	}
	
	
	
	public static Kupac getKupacByU(String korisnicko) {
		
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from kupci where koriscnicko =?");
			ResultSet rs = ps.executeQuery();
			if(rs.next()) {
				String ime = String.valueOf(rs.getString(1));
				String prezime = String.valueOf(rs.getString(2));
				String pol = String.valueOf(rs.getString(3));
				String korisnickoIme = String.valueOf(rs.getString(4));
				String lozinka = String.valueOf(rs.getString(5));
				String adresa = String.valueOf(rs.getString(6));
				String brTelefona = String.valueOf(rs.getString(7));
				String id = String.valueOf(rs.getString(8));
				
				return new Kupac(ime, prezime,pol,korisnickoIme,lozinka,adresa,brTelefona, id);
			}
		//	con.close();
		}catch(Exception e) {e.printStackTrace();}
		return null;
		
	}
	
	public static boolean createKupac(Kupac d) {

		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("insert into kupci values(?,?,?,?,?,?,?,?)");

			ps.setString(1, d.getIme());
			ps.setString(2, d.getPrezime());
			ps.setString(3, d.getPol());
			ps.setString(4, d.getKorisnicko());
			ps.setString(5, d.getLozinka());
			ps.setString(6, d.getAdresa());
			ps.setString(7, d.getBrTelefona());
			ps.setString(8, d.getId());
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean updateKupac(Kupac d) {
		int status = 0;
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("update kupci set ime=?, prezime=?, pol=?, korisnicko=?, lozinka=?, adresa=?, brojTelefona=? where id=? ");
			ps.setString(1, d.getIme());
			ps.setString(2, d.getPrezime());
			ps.setString(3, d.getPol());
			ps.setString(4, d.getKorisnicko());
			ps.setString(5, d.getLozinka());
			ps.setString(6, d.getAdresa());
			ps.setString(7, d.getBrTelefona());
			ps.setString(8, d.getId());
			return ps.executeUpdate() == 1;
			//con.close();
		}catch(Exception e) {e.printStackTrace();}
		return false;
	}
	
	public static boolean deleteKupac(String id) {
		
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("delete from kupci where id=?");
			ps.setString(1, id);
			return ps.executeUpdate() == 1;
			
		}catch(Exception e) {e.printStackTrace();}
		//return status;
		return false;
	}

	public static Kupac getKupacID(String id) {
		Connection con = Connect.getConnection();
		try {
			PreparedStatement ps = con.prepareStatement("select * from kupci where id =?");
			ps.setString(1, id);
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				//Role role = Role.valueOf(rset.getString(1));
				String ime = String.valueOf(rs.getString(1));
				String prezime = String.valueOf(rs.getString(2));
				String pol = String.valueOf(rs.getString(3));
				String korisnicko = String.valueOf(rs.getString(4));
				String lozinka = String.valueOf(rs.getString(5));
				String adresa = String.valueOf(rs.getString(6));
				String brTelefona = String.valueOf(rs.getString(7));
				String idKupca = String.valueOf(rs.getString(8));
				
				return new Kupac(ime, prezime, pol, korisnicko, lozinka, adresa, brTelefona, idKupca);


			}
			//con.close();
		}catch(Exception e) {e.printStackTrace();}
		return null;
	}
	
	/*
	public static Kupac loginKupac(String korisnickoIme, String sifra) {
		for (Kupac kupac : kupci) {
			if(kupac.getKorisnicko().equalsIgnoreCase(korisnickoIme) && kupac.getLozinka().equalsIgnoreCase(sifra)) {
				return kupac;
			}else {
				System.out.print("nece moci");
			}
		}
		return null;
	}
	
	public void removeKupac(Kupac kupac) {
		
		kupci.remove(kupac);
	
	}
	public Kupac getKupac(String korisnicko) {
		// TODO Auto-generated method stub
		for(Kupac kupac : kupci) {
			if(kupac.getKorisnicko().equals(korisnicko)) {
				return kupac;
			}			
		}
		return null;
	} 
	*/
	
}