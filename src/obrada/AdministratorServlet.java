package obrada;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.AdminDAO;
import DAO.DostavljacDAO;
import DAO.JeloDAO;
import DAO.KupacDAO;
import DAO.RestoranDAO;
import modeli.Administrator;
import modeli.Artikal;
import modeli.Dostavljac;
import modeli.Kupac;
import modeli.Restoran;


/**
 * Servlet implementation class AdministratorServlet
 */
@WebServlet("/AdministratorServlet")
public class AdministratorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdministratorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }


	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		ArrayList<Restoran> restorani = RestoranDAO.getAllRestoran();
		System.out.print(restorani);
		String restoraniJson = new Gson().toJson(restorani);
 
		ArrayList<Kupac> kupci = KupacDAO.getAllKupci();
		System.out.print(restorani);
		String kupciJson = new Gson().toJson(kupci);
		
		ArrayList<Artikal> jela = JeloDAO.getAllArtikal();
		System.out.print(restorani);
		String jelaJson = new Gson().toJson(jela);

		ArrayList<Administrator> admini = AdminDAO.getAllAdministratori();
		System.out.print(admini);
		String adminiJson = new Gson().toJson(admini);
		
		ArrayList<Dostavljac> dostavljaci = DostavljacDAO.getAllDostavljaci();
		System.out.print(dostavljaci);
		String dostavljaciJson = new Gson().toJson(dostavljaci);
		
		String id = request.getParameter("id");
		
		Administrator admin = AdminDAO.getAdministratorID(id);
		String adminJson = new Gson().toJson(admin);
		
		System.out.print("\n");
		System.out.print(admin);
		System.out.print("\n");
		String Json = "["+restoraniJson+","+jelaJson+","+kupciJson+","+dostavljaciJson+","+adminiJson+","+adminJson+"]";
	    
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    
	    response.getWriter().write(Json);
	    
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String ime = request.getParameter("ime");
					System.out.println(ime);
					String prezime = request.getParameter("prezime");
					System.out.println(prezime);
					String pol = request.getParameter("pol");
					String korisnicko = request.getParameter("korisnicko");
					String lozinka= request.getParameter("lozinka");
					String JMBG = request.getParameter("JMBG");
					Double plata= Double.parseDouble(request.getParameter("plata"));
					String id = request.getParameter("id");
					
					Administrator administrator = new Administrator(ime, prezime,pol, korisnicko,lozinka,JMBG, plata, id);
					
					
					
					AdminDAO.createAdministrator(administrator);
				}
				case "update": {
					
					String ime = request.getParameter("ime");
					String prezime = request.getParameter("prezime");
					String pol = request.getParameter("pol");
					String korisnicko = request.getParameter("korisni");
					String lozinka= request.getParameter("lozinka");
					String JMBG = request.getParameter("JMBG");
					Double plata= Double.parseDouble(request.getParameter("plata"));
					String id = request.getParameter("id");
					
					Administrator admin = AdminDAO.getAdministratorID(id);
					
					admin.setIme(prezime);
					admin.setPrezime(prezime);
					admin.setPol(pol);
					admin.setKorisnicko(korisnicko);
					admin.setLozinka(lozinka);
					admin.setJMBG(JMBG);
					admin.setPlata(plata);
					admin.setId(id);

					
				}
				case "delete": {
					
					String id = request.getParameter("id");
					AdminDAO.deleteAdministrator(id);
					
				}
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		
	}

}
