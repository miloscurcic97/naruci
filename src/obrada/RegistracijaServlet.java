package obrada;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.servlet.RequestDispatcher;

import DAO.KupacDAO;
import modeli.Kupac;

/**
 * Servlet implementation class RegistracijaServlet
 */
@WebServlet("/RegistracijaServlet")
public class RegistracijaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    public RegistracijaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String ime = request.getParameter("ime");
		String prezime = request.getParameter("prezime ");
		String pol = request.getParameter("pol");
		String korisnicko = request.getParameter("korisnicko");
		String lozinka = request.getParameter("lozinka");
		String adresa = request.getParameter("adresa");
		String brTelefona = request.getParameter("brTelefona");

		AtomicInteger ID_GENERATOR = new AtomicInteger(1000);
		String id = String.valueOf(ID_GENERATOR.getAndIncrement());
		
		
		
		if(KupacDAO.getKupacByU(korisnicko)==null) {
			Kupac kupac = new Kupac(ime,prezime,pol,korisnicko,lozinka,adresa,brTelefona, id );
			KupacDAO.createKupac(kupac);
			System.out.print(kupac);
		}else {
			System.out.print("postoji korisnicko ime u sistemu");
			RequestDispatcher rd = request.getRequestDispatcher("RegistracijaKorisnika.html");
	        rd.include(request,response);  
	        
		}
		
	}

}
