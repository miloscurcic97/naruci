package obrada;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.PorudzbinaDAO;
import modeli.Porudzbina;

@WebServlet("/PorudzbinaServlet")
public class PorudzbinaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	//	response.getWriter().append("Served at: ").append(request.getContextPath());
		
		
		String id = (String) request.getSession().getAttribute("loggedUserId");
		System.out.print("\n");
		System.out.print(id);
		System.out.print("\n");
		
		ArrayList<Porudzbina> porudzbine = PorudzbinaDAO.getPorudzbinaKupacID(id);
		String json = new Gson().toJson(porudzbine);
		

		
		System.out.print(json);
		
		response.setContentType("application/json");
	    response.setCharacterEncoding("UTF-8");
	    response.getWriter().write(json);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
