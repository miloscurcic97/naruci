package obrada;

import java.io.IOException;
import java.util.Map;

import modeli.Osoba;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.AdminDAO;
import DAO.DostavljacDAO;
import DAO.KupacDAO;

import modeli.Administrator;
import modeli.Dostavljac;
import modeli.Kupac;
/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request,response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String korisnicko = request.getParameter("username");
		String sifra = request.getParameter("password");
		String type = request.getParameter("type");
		
		
		//ServletContext context = getServletContext();
		//ovde dodati type pa grananje za login
		
		System.out.println(korisnicko);
		System.out.println(sifra);	
		System.out.println(type);
	
		if(type.equalsIgnoreCase("administrator")) {
			
			Administrator administator = AdminDAO.getAdministrator(korisnicko, sifra);
			if (administator==null) {
				System.out.println("nema");
			}else {
				System.out.println(administator);
				System.out.println("Prijavljen administator");
				
				request.getSession().setAttribute("loggedAdminKorisnicko", administator.getKorisnicko());
				request.getSession().setAttribute("loggedAdminLozinka", administator.getLozinka());
				
				request.getSession().setAttribute("loggedUserId", administator.getId());
				
				RequestDispatcher rd=request.getRequestDispatcher("AdminTab.html"); 
				rd.include(request, response);
			}
			
		}else if(type.equalsIgnoreCase("dostavljac")) {
			Dostavljac dostavljac = DostavljacDAO.getDostavljac(korisnicko, sifra);
			if (dostavljac==null) {
				System.out.println("nema dostave");
			}else {
				System.out.println(dostavljac);
				System.out.println("Prijavljen diostavljac");
				
				request.getSession().setAttribute("loggedDostavljacKorisnicko", dostavljac.getKorisnicko());
				request.getSession().setAttribute("loggedDostavljacLozinka", dostavljac.getLozinka());
				
				request.getSession().setAttribute("loggedUserId", dostavljac.getId());
				
				RequestDispatcher rd=request.getRequestDispatcher("Dostava.html"); 
				rd.include(request, response);
			}
		
		}else if(type.equalsIgnoreCase("kupac")) {
			Kupac kupac = KupacDAO.getKupac(korisnicko, sifra);
			if (kupac==null) {
				System.out.println("nema");
			}else {
				System.out.println(kupac);
	
				
				request.getSession().setAttribute("loggedKupacKorisnicko", kupac.getKorisnicko());
				request.getSession().setAttribute("loggedKupacLozinka", kupac.getLozinka());
				
				// ovde sa get izvali id, proveren DAO, baza, model ??????????
				
				request.getSession().setAttribute("loggedUserId", kupac.getBrTelefona());
				
				System.out.println("=============");
				// ovde sa get izvali brtelefona, proveren DAO, baza, model ??????????
				
				System.out.println(kupac.getId());
				System.out.println("=============");
				System.out.println(kupac.getKorisnicko());
				
				System.out.println("Prijavljen kupac");
				RequestDispatcher rd=request.getRequestDispatcher("Naruci.html"); 
				rd.include(request, response);
			}
					
		}else {
			RequestDispatcher rd=request.getRequestDispatcher("Login.html");  
	        rd.include(request,response);  
	        System.out.println("neuspesna prijava");
		}

		try {

		
		}catch(Exception e) {e.printStackTrace();}
		
	
	}

}
