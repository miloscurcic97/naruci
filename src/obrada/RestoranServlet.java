package obrada;

import java.io.IOException;
import java.util.ArrayList;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import DAO.RestoranDAO;
import modeli.Restoran;

@WebServlet("/RestoranServlet")
public class RestoranServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
    public RestoranServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
	 
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// TODO Auto-generated method stub
		String naziv = request.getParameter("naziv");
		naziv = (naziv != null? naziv: "");
		System.out.println(naziv);
		
		String adresa = request.getParameter("adresa");
		adresa = (adresa != null? adresa: "");
		System.out.println(adresa);
			
		String kategorija = request.getParameter("kategorija");
		kategorija = (kategorija != null? kategorija: "");
		System.out.println(kategorija);
	
		System.out.println("------------");
		ArrayList<Restoran> restorani = RestoranDAO.getRestoran(kategorija);
		
		System.out.print(restorani);
		System.out.println("\n");
		System.out.println("+++++++++++");
		
		String jsonRestorani = new Gson().toJson(restorani);
		System.out.print(jsonRestorani);
		
		System.out.println("id:");
	    String id = request.getParameter("id");
	    System.out.println("restoran");
	    Restoran restoran = RestoranDAO.getRestoranID(id);	
	    System.out.println(restoran);
	    String jsonRestoran = new Gson().toJson(restoran);
	    
	    String Json = "["+jsonRestorani+","+jsonRestoran+"]";
	    
	    response.setContentType("application/json");   
	    response.setCharacterEncoding("UTF-8");
		response.getWriter().write(Json);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			String action = request.getParameter("action");
			switch (action) {
				case "add": {
					String naziv = request.getParameter("naziv");
					
					String adresa = request.getParameter("adresa");
					String kategorija = request.getParameter("kategorija");
					String id = request.getParameter("id");
					
					Restoran restoran = new Restoran(naziv, adresa, kategorija,id);
					
					System.out.println("\n");
					System.out.println(restoran);
					System.out.println("\n");
					RestoranDAO.createRestoran(restoran);
				}
				case "update": {
					//gregors	novosadskog sajma 56	domaca kuhinja
//Restoran [naziv=chi na, adresa=joa minga 224, kategorija=kineska kuhinja kuhinja, id=888888888]
					String naziv = request.getParameter("naziv");
					String adresa = request.getParameter("adresa");
					String kategorija = request.getParameter("kategorija");
					String id = request.getParameter("id");
					
					Restoran restoran = RestoranDAO.getRestoranID(id);
					
					System.out.print(restoran);
					
					Restoran restoranUpdate = new Restoran(naziv, adresa, kategorija, id);
					
					RestoranDAO.updateRestoran(restoranUpdate);
				
				}
				case "delete": {
					
					String id = request.getParameter("id");
					RestoranDAO.deleteRestoran(id);
					
				}
			}
			
		}catch (Exception ex) {
			ex.printStackTrace();
		}
		
		RequestDispatcher rd=request.getRequestDispatcher("Restoran.html");  
        rd.include(request,response);  
	
	}
	
	

}
