package modeli;

public class Artikal {
	private String restoran;
	private String naziv;
	private double cena;
	private String opis;
	private double kolicina;
	private String id;

	public Artikal() {
		this.restoran = "";
		this.naziv = "";
		this.cena = 0;
		this.opis = "";
		this.kolicina = 0;
		this.id = "";
	}
	public Artikal(String restoran, String naziv, Double cena, String opis, Double kolicina, String id) {
		this.restoran =restoran;
		this.naziv = naziv;
		this.cena = cena;
		this.opis = opis;
		this.kolicina = kolicina;
		this.id = id;
	}
	public Artikal(Artikal original) {
		this.restoran = original.restoran;
		this.naziv = original.naziv;
		this.cena = original.cena;
		this.opis = original.opis;
		this.kolicina = original.kolicina;
		this.id = original.id;
	}

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRestoran() {
		return restoran;
	}
	public void setRestoran(String restoran) {
		this.restoran = restoran;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public double getCena() {
		return cena;
	}
	public void setCena(double cena) {
		this.cena = cena;
	}
	public String getOpis() {
		return opis;
	}
	public void setOpis(String opis) {
		this.opis = opis;
	}
	public double getKolicina() {
		return kolicina;
	}
	public void setKolicina(double kolicina) {
		this.kolicina = kolicina;
	}
	@Override
	public String toString() {
		return "Artikal [restoran=" + restoran + ", naziv=" + naziv + ", cena=" + cena + ", opis=" + opis
				+ ", kolicina=" + kolicina + ", id=" + id + "]";
	}
	
}
