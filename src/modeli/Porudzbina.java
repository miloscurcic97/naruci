package modeli;

import java.rmi.server.UID;

public class Porudzbina {
	private String jelo;
	private String datum;
	private String dostavljac;
	private String kupac;
	private String restoran;
	private int broj;
	private String stanje;	
	private double cena;
	private String id;

	public Porudzbina() {
		this.jelo = "";
		this.datum = "";
		this.dostavljac = "";
		this.kupac = "";
		this.restoran = "";
		this.broj = 0;
		this.stanje = "";
		this.cena = 0;
		this.id = "";
	}
	
	public Porudzbina(String jelo,String datum,String dostavljac,String kupac,String restoran,int broj,String stanje, double cena,String uid) {
		this.jelo = jelo;
		this.datum = datum;
		this.dostavljac = dostavljac;
		this.kupac = kupac;
		this.restoran = restoran;
		this.broj = broj;
		this.stanje = stanje;
		this.cena = cena;
		this.id = uid;
	}
	public Porudzbina (Porudzbina original){
		this.jelo = original.jelo;
		this.datum = original.datum;
		this.dostavljac = original.dostavljac;
		this.kupac = original.kupac;
		this.restoran = original.restoran;
		this.broj = original.broj;
		this.stanje = original.stanje;
		this.cena = original.cena;
		this.id = original.id;
	}

	public String getJelo() {
		return jelo;
	}

	public void setJelo(String jelo) {
		this.jelo = jelo;
	}

	public String getDatum() {
		return datum;
	}

	public void setDatum(String datum) {
		this.datum = datum;
	}

	public String getDostavljac() {
		return dostavljac;
	}

	public void setDostavljac(String dostavljac) {
		this.dostavljac = dostavljac;
	}
	public String getStanje() {
		return null;
	}
	public void setStanje(String stanje) {
		this.stanje = stanje;
	}

	public String getKupac() {
		return kupac;
	}

	public void setKupac(String kupac) {
		this.kupac = kupac;
	}

	public String getRestoran() {
		return restoran;
	}

	public void setRestoran(String restoran) {
		this.restoran = restoran;
	}

	public int getBroj() {
		return broj;
	}
	public void setBroj(int broj) {
		this.broj = broj;
	}

	
	public Double getCena() {
		return cena;
	}


	public void setCena(Double cena) {
		this.cena = cena;
	}
	
		public String getId() {
		return id;
	}


	public void setId(String id) {
		this.id = id;
	}
	@Override
	public String toString() {
		return "Porudzbina [jelo=" + jelo + ", datum=" + datum + ", dostavljac=" + dostavljac + ", kupac=" + kupac
				+ ", restoran=" + restoran + ", broj=" + broj + ", stanje=" + stanje + ", cena=" + cena +  ", id=" + id +  "]";
	}


	
}
