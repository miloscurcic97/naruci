package modeli;

public class Dostavljac extends Osoba {
	private String registracija;
	private String tipVozila;
	private String id;
	
	public Dostavljac() {
		this.registracija = "";
		this.tipVozila = "";
		this.id = "";
	}

	public Dostavljac(String ime, String prezime,String pol,String korisnicko,String lozinka, String registracija, String tipVozila,String id) {
		super(ime,prezime,pol,korisnicko,lozinka);
		this.registracija = registracija;
		this.tipVozila = tipVozila;
		this.id = id;
	}
	public Dostavljac(Dostavljac original){
		super(original);
		this.registracija = original.registracija;
		this.tipVozila = original.tipVozila;
		this.id = original.id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String getRegistracija() {
		return registracija;
	}
	public void setRegistracija(String registracija) {
		this.registracija = registracija;
	}
	public String getTipVozila() {
		return tipVozila;
	}
	public void setTipVozila(String tipVozila) {
		this.tipVozila = tipVozila;
	}
	@Override
	public String toString() {
		return "Dostavljac [registracija=" + registracija + ", tipVozila=" + tipVozila + ", id=" + id + ", ime=" + ime
				+ ", prezime=" + prezime + ", pol=" + pol + ", korisnicko=" + korisnicko + ", lozinka=" + lozinka + "]";
	}

}

	
