package modeli;


public class Restoran {
	
	private String naziv;
	
	private String adresa;
	
	private String kategorija;
	
	private String id;
	
//String naziv, String adresa, String kategorija,String id
	
	public Restoran(){
		this.naziv = "";
		this.adresa = "";
		this.kategorija = "";
		this.id = "";
		
	
	}
	public Restoran(String naziv,String adresa,String  kategorija,String id){
		this.naziv = naziv;
		this.adresa = adresa;
		this.kategorija = kategorija;
		this.id = id;
		
	}
	public Restoran(Restoran original) {
		this.naziv = original.naziv;
		this.adresa = original.adresa;
		this.kategorija = original.kategorija;
		this.id = original.id;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNaziv() {
		return naziv;
	}
	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}
	public String getKategorija() {
		return kategorija;
	}
	public void setKategorija(String kategorija) {
		this.kategorija = kategorija;
	}
	public String getAdresa() {
		return adresa;
	}
	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}	
	@Override
	public String toString() {
		return "Restoran [naziv=" + naziv + ", adresa=" + adresa + ", kategorija=" + kategorija + ", id=" + id + "]";
	}

}
